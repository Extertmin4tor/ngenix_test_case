import os
import random
import uuid
import shutil
from contextlib import suppress

from lxml import etree

from conf import ARCHIVE_DIR, TMP_DIR


class DataGenerator:
    def __init__(self, number_of_archives: int, number_of_xml: int):
        """
        Args:
            number_of_archives: Количество архивов, которые необходимо сосздать
            number_of_xml: Количество Xml в каждом архиве
        """
        self._archives_number = number_of_archives
        self._xmls_number = number_of_xml

    @staticmethod
    def _create_archive_directory():
        """Очистить существующую и создать директорию для архивов."""
        with suppress(FileNotFoundError):
            shutil.rmtree(ARCHIVE_DIR)
        os.makedirs(ARCHIVE_DIR)

    @staticmethod
    def _generate_xml() -> bytes:
        """Сгенерировать Xml."""
        root = etree.Element("root")
        var1 = etree.SubElement(root, "var")
        var1.attrib['name'] = 'id'
        var1.attrib['value'] = str(uuid.uuid4())
        var2 = etree.SubElement(root, "var")
        var2.attrib['name'] = 'level'
        var2.attrib['value'] = str(random.randint(1, 100))
        objects = etree.SubElement(root, "objects")

        for _ in range(random.randint(1, 10)):
            object_tag = etree.SubElement(objects, "object")
            object_tag.attrib['name'] = str(uuid.uuid4())

        return etree.tostring(root, pretty_print=True)

    def _create_xmls_batch(self) -> None:
        """Создать в файловой системе заданное количество Xml-файлов."""
        for i in range(self._xmls_number):
            with open(f'{TMP_DIR}/xml_{i}.xml', 'wb') as file:
                file.write(self._generate_xml())

    @staticmethod
    def _create_archive(index: int):
        """Сосздать архив на основе созданных Xml-файлов."""
        shutil.make_archive(f"{ARCHIVE_DIR}/{str(index)}", 'zip', TMP_DIR)

    def make_data(self):
        """Сгенерировать архивы с данными."""
        self._create_archive_directory()
        for i in range(self._archives_number):
            os.makedirs(TMP_DIR, exist_ok=True)
            self._create_xmls_batch()
            self._create_archive(i)
            shutil.rmtree(TMP_DIR)


if __name__ == "__main__":
    DataGenerator(50, 100).make_data()
