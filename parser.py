import csv
import os
import shutil
import zipfile
from collections import Generator
from contextlib import suppress
from multiprocessing import Process, Queue, cpu_count

from lxml import etree

from conf import ARCHIVE_DIR, EXTRACTED_DIR, PERIODS_CSV_FILE, OBJECTS_CSV_FILE


class XmlParser:
    def __init__(self, producers_number: int = cpu_count() - 2):
        """
        Args:
            producers_number: Количество процессов-продюссеров
        """
        self._producers_number = producers_number
        self._init_queue = None
        self._periods_queue = None
        self._objects_queue = None

    @staticmethod
    def _populate_workers(job_function, number, *args) -> list:
        """
        Создание прцоессов.

        Args:
            job_function: Метод для выполнения в рамках процесса
            number: Количество процессов в пуле
            args: Аргументы метода
        """
        workers = []
        for _ in range(number):
            process = Process(
                target=job_function, args=args
            )
            workers.append(process)

        return workers

    def _perform_processing(self) -> None:
        """Инициализация и запуск процессов."""
        producers = self._populate_workers(
            self._produce, self._producers_number,
            self._init_queue, self._periods_queue,
            self._objects_queue
        )
        [producer.start() for producer in producers]

        periods_consumer = self._populate_workers(
            self._consume, 1, self._periods_queue, PERIODS_CSV_FILE
        )
        objects_consumer = self._populate_workers(
            self._consume, 1, self._objects_queue, OBJECTS_CSV_FILE
        )
        consumers = periods_consumer + objects_consumer

        [consumer.start() for consumer in consumers]
        [producer.join() for producer in producers]

        self._periods_queue.put(None)
        self._objects_queue.put(None)

        [consumer.join() for consumer in consumers]

    @staticmethod
    def _produce(inputqueue, periods_queue, objects_queue) -> None:
        """Разрабор Xml и запись в соответствующие очереди."""
        while True:
            file_path = inputqueue.get()
            if file_path is None:
                return
            root = etree.parse(file_path)
            identifier = root.xpath('//var[@name = "id"]/@value')[0]
            level = root.xpath('//var[@name = "level"]/@value')[0]
            periods_queue.put((identifier, level))
            for object_tag in root.xpath('//object'):
                objects_queue.put((identifier, object_tag.xpath('@name')[0]))

    @staticmethod
    def _consume(result_queue, output_file_name) -> None:
        """Получение данных из очереди и запись в соответствующий CSV."""
        while True:
            data = result_queue.get()
            if data is None:
                return

            with open(output_file_name, 'a+', newline='') as csv_file:
                writer = csv.writer(
                    csv_file, delimiter=',',
                    quotechar='"', quoting=csv.QUOTE_MINIMAL
                )
                writer.writerow(data)

    @staticmethod
    def _extract_data(file_path: str) -> None:
        """Распаковка архива."""
        os.makedirs(EXTRACTED_DIR, exist_ok=True)
        with zipfile.ZipFile(file_path, "r") as zip_ref:
            zip_ref.extractall(EXTRACTED_DIR)

    def _extract_batch(self) -> Generator:
        """Распаковка всех файлов в директории."""
        for root, _, files in os.walk(ARCHIVE_DIR):
            for file in files:
                self._extract_data(os.path.join(root, file))
                yield

    @staticmethod
    def _clear_csv(file_path: str) -> None:
        """
        Удаление старых CSV, если существуют.

        Args:
            file_path: Путь до CSV-файла
        """
        with suppress(FileNotFoundError):
            os.remove(file_path)

    def parse_parallel(self) -> None:
        """Интерфейс параллельной обработки xml."""
        self._clear_csv(PERIODS_CSV_FILE)
        self._clear_csv(OBJECTS_CSV_FILE)
        for _ in self._extract_batch():
            self._prepare_queues()
            self._perform_processing()
            self._clear_extracted()

    @staticmethod
    def _clear_extracted() -> None:
        """Очистить директорию с распакованными файлами."""
        with suppress(FileNotFoundError):
            shutil.rmtree(EXTRACTED_DIR)

    def _prepare_queues(self) -> None:
        """Подготовка очередей для итерации."""
        self._periods_queue = Queue()
        self._init_queue = Queue()
        self._objects_queue = Queue()
        self._set_file_paths()
        for _ in range(self._producers_number):
            self._init_queue.put(None)

    def _set_file_paths(self) -> None:
        """Инициализируем очередь путями распакованных файлов."""
        for file_name in os.listdir(EXTRACTED_DIR):
            self._init_queue.put(f"{EXTRACTED_DIR}/{file_name}")


if __name__ == "__main__":
    XmlParser().parse_parallel()
