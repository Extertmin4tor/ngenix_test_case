Инициализация:
```bash
pip install -r req.txt
```

Запуск:
```bash
python generator.py && python parser.py
```
